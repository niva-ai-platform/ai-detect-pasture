import tensorflow as tf
from tensorflow import keras


model = keras.models.load_model("./model/testmodel-2022-09-16.h5")


def detect_pasture(file_in, confidence_threshold=0.5):
    image = keras.preprocessing.image.load_img(
        file_in, target_size=(400, 400)
    )
    image_array = keras.preprocessing.image.img_to_array(image)
    image_reshaped = tf.expand_dims(image_array, 0)  # Create batch axis

    prediction = model.predict(image_reshaped)
    confidence = 1.0 - float(prediction[0][0])

    pasture_detected = confidence >= confidence_threshold

    tags = ['detect-pasture'] if pasture_detected else []

    output_data = {
        'tags': tags,
        'results': {
            'targetDetected': pasture_detected,
            'maxConfidence': confidence
        }
    }
    return output_data
