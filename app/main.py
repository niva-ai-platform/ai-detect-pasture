from detect_pasture import detect_pasture
from ai_python_detect_wrapper_library import AiDetectWrapper

ai_detect_wrapper = AiDetectWrapper()
ai_detect_wrapper.register_service()
ai_detect_wrapper.create_download_dir()
ai_detect_wrapper.initialise_app(detect_pasture)

app = ai_detect_wrapper.get_app()
