import sys
sys.path.append('..')

from os import listdir
from os.path import isfile, join
from app.detect_pasture import detect_pasture

IMAGE_PATH = '../test/samples/Clean Permanent Pasture'

sample_images = [
    join(IMAGE_PATH, image_name) for image_name in listdir(IMAGE_PATH) if isfile(join(IMAGE_PATH, image_name) and image_name != '.DS_Store')
    ]

if __name__ == '__main__':
    avg_confidence = 0
    print(f'Image directory tested: {IMAGE_PATH}')
    for image in sample_images:

        print()
        print(image)
        prediction = detect_pasture(image)
        print(prediction)

        avg_confidence += prediction['results']['maxConfidence']

    avg_confidence /= len(sample_images)

    print(f'\n\n\nNumber of images tested: {len(sample_images)}')
    print(f'Average confidence: {avg_confidence}')
