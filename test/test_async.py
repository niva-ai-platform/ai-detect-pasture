import asyncio
import aiohttp
import time


HOST = 'http://localhost:8000/detect-pasture'
AUTHORIZATION_TOKEN = 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJnQ2V6UXZIbEpCYkpKR2lsS0Jjb3RMOVdPZjVRZmh1N0hSZFZ5b2VSUWRzIn0.eyJleHAiOjE2NjQyOTg4NjQsImlhdCI6MTY2NDI3MDA2NCwiYXV0aF90aW1lIjoxNjY0MjcwMDYzLCJqdGkiOiI0NTllZjIwZS00OWY2LTRkNmQtOTY1Mi02MWJiOTEzMjgyNGYiLCJpc3MiOiJodHRwczovL25pdmEtYWkta2V5Y2xvYWstZGV2LnRzc2cub3JnL3JlYWxtcy9uaXZhLWFpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6ImM3ZDI1YjlkLWZjMzAtNDcyYi04MGEwLTI0OWQ5YWM5M2Y0ZiIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5pdmEtYWktZnJvbnRlbmQiLCJub25jZSI6ImUzODllNTFlLTdmNDItNGVhMS04YTYzLTVmMTUyMmE2ZmZmOCIsInNlc3Npb25fc3RhdGUiOiI4NTk5ZWM3Ni02MjkyLTQyNDctYmM0OS0xODA0MzM1ZTQ4YWIiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsidXBsb2FkLWltYWdlIiwib2ZmbGluZV9hY2Nlc3MiLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIl19fSwic2NvcGUiOiJvcGVuaWQiLCJzaWQiOiI4NTk5ZWM3Ni02MjkyLTQyNDctYmM0OS0xODA0MzM1ZTQ4YWIifQ.bLiV4q04NwRasTa1FGK_p4bbhwC4DBp4O01B6wlhb34hrU_MnfZu2dIP39Rk4JDy6LWnZ97lyDF5ZqSFuLzQahv9-dMHJDbU8ROFBUVRWxIfDLjtvESpRaOcnfxYmVWvTLisMvDQno6N7i_BJkyjhGka5Q9-5i0SmJA3PX6KNncG91p-3wBmw_WjYiP46oOxIzGdr00nnGIpBbNPRZUUNYC1-V3sJ8EqcTacwwbKfliUTH4DXtRWVAWWb_D9JWu1QP0AbCS400Kw9pMn0_5V_GZGV4Xz5d6jr4toF57K11e26FOd1sk7s7nLdT1GK8X02TwBb8CRzeubrPuVk8FNVw'


def get_tests():
    url0 = f'{HOST}/process-image?image_url=https://niva-ai-dev.tssg.org/image-fs/files/detect-pasture-sample-images/1663163598225_detect-pasture-sample-images_dc7f61b1-525b-4a3b-9f30-396789fe1a40_CERT_Clean Permanent Pasture (147).jpg'
    url1 = f'{HOST}/process-image?image_url=https://niva-ai-dev.tssg.org/image-fs/files/detect-pasture-sample-images/1663163585774_detect-pasture-sample-images_e331393f-30c2-4184-83a1-0f7f88de483b_CERT_Clean Permanent Pasture (26).jpg'
    url2 = f'{HOST}/process-image?image_url=https://niva-ai-dev.tssg.org/image-fs/files/detect-pasture-sample-images/1663163587171_detect-pasture-sample-images_e8acd1aa-5a15-41ed-8699-6f6735fdea57_CERT_Clean Permanent Pasture (33).jpg'
    url3 = f'{HOST}/process-image?image_url=https://niva-ai-dev.tssg.org/image-fs/files/detect-pasture-sample-images/1663163588439_detect-pasture-sample-images_903505d6-b650-4207-b002-e7d7e0e44195_CERT_Clean Permanent Pasture (34).jpg'

    # URLs and their expected confidences
    test0 = (url0, 0.5283876657485962)
    test1 = (url1, 0.5819266140460968)
    test2 = (url2, 0.2884026765823364)
    test3 = (url3, 0.3124682307243347)

    tests = [test0, test1, test2, test3]
    
    return tests


async def test_prediction(session, url, expected_conf):
    async with session.get(url) as resp:
        response = await resp.json()
        conf = response['results']['maxConfidence']
        assert conf == expected_conf, f'Expected {expected_conf}, but received {conf}'
        return response


async def main(tests):
    async with aiohttp.ClientSession(headers={'Authorization': AUTHORIZATION_TOKEN}) as session:
        tasks = []
        for url, conf in tests:
            tasks.append(asyncio.ensure_future(test_prediction(session, url, conf)))

        responses = await asyncio.gather(*tasks)
        for response in responses:
            print(response, '\n\n')


if __name__ == '__main__':
    tests = get_tests()
    start_time = time.time()
    asyncio.run(main(tests))
    seconds_taken = time.time() - start_time
    print(f'Tests completed in {seconds_taken} seconds.')
